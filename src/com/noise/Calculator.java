package com.noise;

import java.io.IOException;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Calculator {
    private static Scanner op;
    private static Scanner input;
    private static Scanner cont;

    public static void main(String args[]) throws Exception {
        while (true) {
            getCalc();
        }
    }

    public static void getCalc() throws IOException {
        BufferedReader key = new BufferedReader(new InputStreamReader(System.in));
        double num1 = 0;
        double num2 = 0;
        double res = 0;
        double ans = 0;
        double l1;
        double l2;

        System.out.println("Введите первое число:");
        String operation;
        input = new Scanner(System.in);
        String f = key.readLine();
        try {
            num1 = Float.parseFloat(f);
        } catch (NumberFormatException e) {
            System.err.println("Ошибка ввода");
            System.out.println("Для рестарта введите: 'Yes' \nДля выхода нажмите любую клавишу");
            cont = new Scanner(System.in);
            String error;
            error = cont.next();
            if (error.equals("Yes")) {
                getCalc();
            } else {
                System.out.println("Работа завершена, Спасибо!");
                System.exit(0);
            }
        }

        while (true) {

            op = new Scanner(System.in);
            System.out.println("Выберите операцию:");
            operation = op.next();

            System.out.println("Введите второе число:");
            String s = key.readLine();
            try {
                num2 = Float.parseFloat(s);
            } catch (NumberFormatException e) {
                System.err.println("Ошибка ввода");
                System.out.println("Для рестарта введите: 'Yes' \nДля выхода нажмите любую клавишу");
                cont = new Scanner(System.in);
                String error;
                error = cont.next();
                if (error.equals("Yes")) {
                    getCalc();
                } else {
                    System.out.println("Работа завершена, Спасибо!");
                    System.exit(0);
                }
            }


            if (operation.equals("+")) {
                res = num1 + num2;
            }
            if (operation.equals("-")) {
                res = num1 - num2;
            }
            if (operation.equals("/")) {
                if (num2 == 0) {
                    System.out.println("Деление на ноль");
                } else {
                    res = num1 / num2;
                }

            }
            if (operation.equals("*")) {
                res = num1 * num2;
            }
            if (operation.equals("%")) {
                res = num1 * 100 / num2;
            }
            if (operation.equals("^")) {
                ans = Math.pow(num1, num2);
                res = ans;
            }
            if (operation.equals("log")) {
                l1 = Math.log(num1);
                l2 = Math.log(num2);
                res = l1 / l2;
            }

            System.out.println("Ответ:" + res);
            System.out.println("Хотите продолжить вычисления? Введите: 'Yes' \nХотите начать сначала? Введите: 'New'\nДля выхода нажмите любую клавишу");
            cont = new Scanner(System.in);
            operation = cont.next();

            if (operation.equals("Yes")) {
                num1 = res;
            } else if (operation.equals("New")) {
                getCalc();
            } else {
                System.out.println("Работа завершена, Спасибо!");
                System.exit(0);
            }
        }
    }
}




