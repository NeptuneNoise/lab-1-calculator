import com.noise.Calculator;
import org.junit.*;
import org.junit.rules.ExpectedException;

public class Testl1 {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void start() {
        System.out.println("Запуск тестов...");

    }

    @Test
    public void testAdd() {
        //Тест на сложение
        Assert.assertEquals(15, Calculator.add(10, 5), 0);
        Assert.assertEquals(200, Calculator.add(100, 100), 0);
        Assert.assertEquals(0, Calculator.add(-100, 100), 0);
        Assert.assertEquals(1, Calculator.add(0.5, 0.5), 0);
        Assert.assertEquals(1.75, Calculator.add(0.5, 1.25), 0);
        Assert.assertEquals(-2, Calculator.add(10, -12), 0);
    }

    @Test
    public void testSub() {
        //Тест на вычитание
        Assert.assertEquals(10, Calculator.sub(20, 10), 0);
        Assert.assertEquals(200, Calculator.sub(100, -100), 0);
        Assert.assertEquals(0, Calculator.sub(100, 100), 0);
        Assert.assertEquals(1, Calculator.sub(1.5, 0.5), 0);
        Assert.assertEquals(-0.75, Calculator.sub(0.5, 1.25), 0);
        Assert.assertEquals(2, Calculator.sub(-10, -12), 0);
    }

    @Test
    public void testDiv() {
        //Тест на деление
        Assert.assertEquals(10, Calculator.div(100, 10), 0);
        Assert.assertEquals(-1, Calculator.div(100, -100), 0);
        Assert.assertEquals(1, Calculator.div(100, 100), 0);
        Assert.assertEquals(3, Calculator.div(1.5, 0.5), 0);
        Assert.assertEquals(-0.4, Calculator.div(0.5, -1.25), 0);
        Assert.assertEquals(0.8333333333333334, Calculator.div(-10, -12), 0);
    }

    @Test
    public void testMlp() {
        //Тест на умножение
        Assert.assertEquals(25, Calculator.mlp(5, 5), 0);
        Assert.assertEquals(-10000, Calculator.mlp(100, -100), 0);
        Assert.assertEquals(0, Calculator.mlp(100, 0), 0);
        Assert.assertEquals(0.75, Calculator.mlp(1.5, 0.5), 0);
        Assert.assertEquals(-0.625, Calculator.mlp(-0.5, 1.25), 0);
        Assert.assertEquals(120, Calculator.mlp(-10, -12), 0);
    }

    @Test
    public void testPer() {
        //Тест на расчет процента
        Assert.assertEquals(90, Calculator.per(45, 50), 0);
        Assert.assertEquals(50, Calculator.per(-25, -50), 0);
        Assert.assertEquals(100, Calculator.per(50, 50), 0);
        Assert.assertEquals(48.5, Calculator.per(24.25, 50), 0);
        Assert.assertEquals(0, Calculator.per(0, 100), 0);
    }

    @Test
    public void testLog() {
        //Тест на расчет логарифма
        Assert.assertEquals(8, Calculator.log(16, 2), 0);
        Assert.assertEquals(-4, Calculator.log(-16, 4), 0);
        Assert.assertEquals(71.333333333333333, Calculator.log(160.5, 2.25), 0);
        Assert.assertEquals(6.384615384615385, Calculator.log(-166, -26), 0);
        Assert.assertEquals(-716.5178571428571, Calculator.log(1605, -2.24), 0);
    }
    @AfterClass
    public static void finish() {
        System.out.println("Тесты Завершенны...");
    }

}
